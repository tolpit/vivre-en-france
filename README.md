## Carte de France

### Sources des données :

- Liste des réacteurs nucléaires :
	- Source : [https://www.data.gouv.fr/en/datasets/liste-des-reacteurs-nucleaires-en-france-nd/](https://www.data.gouv.fr/en/datasets/liste-des-reacteurs-nucleaires-en-france-nd/)
- Liste des différentes gares en fonctionnement :
	- Source : [https://www.data.gouv.fr/fr/datasets/liste-des-gares/](https://www.data.gouv.fr/fr/datasets/liste-des-gares/)
	- Exclusion des gares de fret en fonction des données récupérées
- Cinémas :
	-  Source : [https://www.data.gouv.fr/fr/datasets/liste-des-etablissements-cinematographiques-actifs-1/](https://www.data.gouv.fr/fr/datasets/liste-des-etablissements-cinematographiques-actifs-1/) 
	- Ajout d'une géolocalisation (latitude et longitude) de chacun des cinémas fournis par le service [https://geo.api.gouv.fr](https://geo.api.gouv.fr/adresse)

### Objectifs 
- Chercher des lieux répondants à différents critères, modulables, dans le but d'y habiter