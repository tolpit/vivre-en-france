const fs = require('fs');
const fetch = require('node-fetch');
let cinemas = require('./assets/data/cinemas.json');

// gares = gares.records.map(item => {
//     item = {...item,...item.fields}

//     delete item.fields

//     delete item.datasetid
//     delete item.record_timestamp
//     delete item.voyageurs
//     delete item.y_l93
//     delete item.x_l93
//     delete item.code_uic
//     delete item.pk
//     delete item.rg_troncon
//     delete item.fret
//     delete item.y_wgs84
//     delete item.x_wgs84
//     delete item.geometry
//     delete item.recordid

//     return item
// })

// fs.writeFileSync("./assets/data/gares.sncf.json", JSON.stringify(gares), 'utf8');

(async function() {
    for await (let index of cinemasGenerator()) {
        const cinema = cinemas[index];

        await fetch(`https://api-adresse.data.gouv.fr/search/?q=${cinema['NOM ETABLISSEMENT']} ${cinema['COMMUNE']}`)
            .then(res => res.json())
            .then(res => {
                if(res.features && res.features[0] && res.features[0].geometry && res.features[0].geometry.coordinates) {
                    cinemas[index].coordinates = res.features[0].geometry.coordinates;    

                    console.log(index)                
                }
            }) 
            .catch(err => console.log(err, cinemas[index]))       
    }

    fs.writeFileSync("./assets/data/cinemas.geo.json", JSON.stringify(cinemas), 'utf8');
})();

async function* cinemasGenerator() {
    var i = 0;
    while (i < cinemas.length) {
        yield i++;
    }
}