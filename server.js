const express   = require('express')
const fs        = require('fs')
const path      = require('path')
const jsonfile  = require('jsonfile')
const bodyParser = require('body-parser')
const cors = require('cors')
const leboncoin = require('leboncoin-api')
const md5 = require('md5')

const gares = require('./assets/data/gares.sncf.json')

const app = express()

let maxOffset

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/assets', express.static(path.join(__dirname, 'assets')))

app.get('/zones-inondables', async (req, res) => {
    const bretagne = await jsonfile.readFile('./assets/data/zones_inondables/azibretagne.json')

    return res.json(bretagne.features)
})

app.post('/search', async (req, res) => {
    const garesIds      = req.body.garesIds
    const gareDistance  = req.body.gareDistance

    let garesList = gares.filter(gare => ~garesIds.indexOf(gare.idgaia) && !!gare.c_geo && !!gare.c_geo[0])

    maxOffset = garesList.length

    const fileName = md5(garesList) + '.' + (new Date(new Date().toJSON().slice(0,10).replace(/-/g,'/'))).getTime() + '.json'
    const filePath = path.join(__dirname, 'assets', 'search', fileName)

    if(fs.existsSync(filePath)) {
        const results = await jsonfile.readFile(filePath)

        if(!!results.length) return res.json(results)
    }
    else {
        fs.writeFileSync(filePath, JSON.stringify([]), 'utf8');
    }

    let searchResults = []

    for await (let index of maxOffsetGenerator()) {
        const gare = garesList[index]

        console.log(index)

        let gareSearchResults = await doSearch(gare, 1, gareDistance)

        gareSearchResults = proccessSearchResults(gareSearchResults)

        searchResults = searchResults.concat(gareSearchResults)

        jsonfile.writeFile(filePath, searchResults)
    }

    res.json(searchResults)
})

async function* maxOffsetGenerator() {
    var i = 1;
    while (i < maxOffset) {
        yield i++;
    }
}

function doSearch(gare, page = 1, gareDistance = 10) {
    let results = []

    return new Promise(resolve => {
        let search = new leboncoin.Search()
            .setPage(page)
            .setQuery("terrain")
            .setCategory("ventes_immobilieres")
            .setArea({
                "lat": gare.c_geo[0], 
                "lng": gare.c_geo[1], 
                "radius": gareDistance * 1000 
            })
            .addSearchExtra('square', { min: 400, max: 100000 }) 

        search.run().then(data => {
            results = data.results

            if(data.pages > data.page) {
                doSearch(gare, page + 1, gareDistance)
                    .then(moreData => {
                        results = results.concat(moreData)
                        resolve(results)
                    })
            }
            else {
                resolve(results)
            }
        })
    })
}

function proccessSearchResults(results) {
    results = results.filter(item => {
        if(item.price > 50000) return false
        return true;
    })

    return results
}

app.listen(7500)