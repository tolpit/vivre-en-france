import Component from '../core/component'
import {API_URL} from '../utils/global'

export default class SearchComponent extends Component {

  static selector() { return ".send__search" }

  
  constructor(element) {
    super(element)

    this.attchEvents()
  }

  attchEvents() {
    this.element.addEventListener('click', this.send.bind(this), false)
  }

  send(event) {
      event.preventDefault()

      fetch(`${API_URL}/search`, {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          garesIds: this.$store.garesIds,
          gareDistance: this.$store.filters.train
        })
      })
        .then(res => res.json())
        .then(res => {
            console.log(res)
        })
        .catch(err => console.error(err))
    
  }

}

Component.append(SearchComponent)