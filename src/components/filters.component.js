import Component from '../core/component'
import {getCinemas,getZonesInondables,getMonuments} from '../api/api'

export default class FiltersComponent extends Component {

  static selector() { return ".filter__list" }

  static watch() { return "filters" }

  constructor(element) {
    super(element)

    this.filters = {
      nuclear: {
        selector: "#nuclear_distance"
      },
      train: {
        selector: "#train_distance"
      },
      flood: {
        selector: "#flood_distance",
        data: getZonesInondables,
        source: 'floods'
      },
      cinema: {
        selector: "#cinema_distance",
        data: getCinemas,
        source: 'cinemas'
      },
      historique: {
        selector: "#historique_distance",
        data: getMonuments,
        source: 'monuments'
      }
    }

    this.attachEvents()
  }

  attachEvents() {
    for(let filterName in this.filters) {
      const filterHandler = this.handleFilter.bind(this)

      this.filters[filterName].element = this.element.querySelector(this.filters[filterName].selector)
      this.filters[filterName].handler = filterHandler;

      this.filters[filterName].element.addEventListener('change', filterHandler, false);
    }
  }

  clearEvents() {
    for(let filterName in this.filters) {
      this.filters[filterName].element.removeEventListener('change', this.filters[filterName].handler);
      this.filters[filterName].handler = null
    }
  }

  handleFilter(event) {
    const target   = event.target
    let name       = null
    let value      = null
    let dataBefore = () => new Promise(r => r())

    for(let filterName in this.filters) {
      if(this.filters[filterName].selector === `#${target.id}`) {
        name = filterName
        
        if(this.filters[filterName].hasOwnProperty('data') && typeof this.filters[filterName].data === 'function') {
          dataBefore = this.filters[filterName].data
        }
      }
    }

    dataBefore()
      .then(data => {
        if(!!data) this.$store[this.filters[name].source] = data
        
        switch(target.id) {
          case "promotion_filter_dates":
              this.filters.dates = parseInt(target.value, 10);
          break;

          default:
              value = parseInt(target.value, 10);
          break;
      }

      this.$store.filters[name] = value

      this.$store.update('filters')
    })
  }

  render() {
    
  }

}

Component.append(FiltersComponent);