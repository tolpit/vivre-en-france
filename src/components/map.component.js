import kdbush from 'kdbush'
import geokdbush from 'geokdbush'
import Component from '../core/component'
import localforage from 'localforage'

import {BASE_TILE,TYPE_TILE,MAP_TOKEN} from '../utils/global'
import {long2tile,lat2tile} from '../utils/map'
import {preloadImages} from '../utils/image'

const layers = [L.tileLayer(`${BASE_TILE}{id}/{z}/{x}/{y}.png?access_token={accessToken}`, {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: TYPE_TILE,
    accessToken: MAP_TOKEN
})]

export default class MapComponent extends Component {

  static selector() { return "#map" }

  static watch() { return ["points", "filters", "gares", "centrales"] }

  constructor(element) {
    super(element)

    this.map = L.map('map', {
        center: [46.448,2.241],
        zoom: 6,
        layers
    })

    this.markers = {}

    this.appendControl()

    if(this.element.hasAttribute('data-offline')) {
        this.preloader()
    }
  }

  appendControl() {
    // FeatureGroup is to store editable layers
    this.drawnItems = new L.FeatureGroup()
    
    this.map.addLayer(this.drawnItems)

    localforage.getItem('zones')
        .then(zones => {
            if(!!zones) {
                L.geoJSON(zones).addTo(this.drawnItems)

                const layers = this.drawnItems.getLayers()
                const mainLayer = layers[0] ? layers[0] : null

                // Center map to the drawned layer
                if(!!mainLayer) {
                    this.map.setView(
                        mainLayer.getBounds().getCenter(), 
                        7, 
                        { animation: true }
                    )
                }
            }
        })
        .catch(err => {
            console.log(err)
        })

    const drawControl = new L.Control.Draw({
        draw: {
            polygon: false,
            marker: false,
            circlemarker: false
        },
        edit: {
            featureGroup: this.drawnItems
        }
    })

    this.map.addControl(drawControl)

    this.map.on(L.Draw.Event.CREATED, e => {
        var type = e.layerType,
            layer = e.layer

        this.drawnItems.addLayer(layer)

        localforage.setItem('zones', this.drawnItems.toGeoJSON())

        this.$store.filters.bounds = this.drawnItems.toGeoJSON()
        this.$store.update('filters')
    })

    this.map.on(L.Draw.Event.DELETED, e => {
        localforage.setItem('zones', this.drawnItems.toGeoJSON())

        this.$store.filters.bounds = this.drawnItems.toGeoJSON()
        this.$store.update('filters')
    })

  }

  render() {
    if(!this.$store.gares.length || !this.$store.centrales.length) return

    let gares     = this.$store.gares.filter(gare => !!gare.c_geo)
    let centrales = this.$store.centrales.filter((centrale, index) => index === this.$store.centrales.findIndex(item => item['Commune Lat'] === centrale['Commune Lat'] && item['Commune long'] === centrale['Commune long']))
    let cinemas   = this.$store.cinemas.filter(cinema => !!cinema.coordinates)
    let floods    = this.$store.floods
    let monuments = this.$store.monuments

    let index = new kdbush(gares, p => p.c_geo[0], p => p.c_geo[1])

    let toRemove = []

    centrales.forEach(centrale => {
        const lat = centrale['Commune Lat']
        const lng = centrale['Commune long']

        const nearest = geokdbush.around(index, lat, lng, null, this.$store.filters.nuclear)

        toRemove = toRemove.concat(nearest.map(gare => gare.idgaia))
    })

    gares = gares.filter(gare => toRemove.indexOf(gare.idgaia) === -1)

    gares = gares.filter(gare => {
        const lat = gare.c_geo[0]
        const lng = gare.c_geo[1]

        const latLng = L.latLng(lat, lng)

        gare.latLng = latLng
        
        let res = true

        this.drawnItems.eachLayer(layer => {
            res = !res ? res : layer.getBounds().contains(latLng)
        })
        
        return res
    })

    if(!!this.$store.filters.cinema) {
        let garesToKeep = []

        cinemas.forEach(cinema => {
            const lat = cinema.coordinates[1]
            const lng = cinema.coordinates[0]

            const nearestCinemas = geokdbush.around(index, lat, lng, null, this.$store.filters.cinema)

            garesToKeep = garesToKeep.concat(nearestCinemas.map(gare => gare.idgaia))
        })

        gares = gares.filter(gare => ~garesToKeep.indexOf(gare.idgaia))
    }

    if(!!this.$store.filters.flood) {
        let floodsFeatures = []

        if(!!floods.features) {
            floods.features.forEach(feature => {
                feature.geometry.coordinates[0].forEach(coords => {
                    floodsFeatures = floodsFeatures.concat(coords)
                })
            })

            const floodIndex = new kdbush(floodsFeatures)

            gares = gares.filter(gare => {
                const lat = gare.c_geo[0]
                const lng = gare.c_geo[1]

                const nearest = geokdbush.around(floodIndex, lng, lat, null, this.$store.filters.flood)

                return !nearest.length
            })
        }
    }

    if(!!this.$store.filters.historique) {
        let monumentsFeatures = []

        if(!!monuments.features) {
            monuments.features.forEach(feature => {
                feature.geometry.coordinates.forEach(coords => {
                    monumentsFeatures = monumentsFeatures.concat(coords)
                })
            })

            const monumentsIndex = new kdbush(monumentsFeatures)

            gares = gares.filter(gare => {
                const lat = gare.c_geo[0]
                const lng = gare.c_geo[1]

                const nearest = geokdbush.around(monumentsIndex, lng, lat, null, this.$store.filters.historique)

                return !nearest.length
            })
        }
    }

    this.updateGares(gares)
  }

  updateGares(gares) {
    const garesIds = gares.map(gare => gare.idgaia)

    this.$store.garesIds = garesIds

    // makers to remove
    for(let id in this.markers) {
        if(!garesIds.some(gareId => gareId === id)) {
            this.map.removeLayer(this.markers[id])
            delete this.markers[id]
        }
    }  

    gares.forEach(gare => {
      const lat = gare.c_geo[0]
      const lng = gare.c_geo[1]

      if(!!this.markers[gare.idgaia]) return

      const marker = L.marker([lat,lng], {
        title: gare.libelle
      }).addTo(this.map)

      this.markers[gare.idgaia] = marker  
    })
  }

  appendCentrales(centrales) {
    centrales.forEach(centrale => {
        const lat = centrale['Commune Lat']
        const lng = centrale['Commune long']

        L.marker([lat,lng], {
            title: centrale['Centrale nucléaire']
        }).addTo(map)

    })
  }

  preloader() {
    // preload part
    this.map.on('moveend', function handleMapMove(e) {
        const zoom      = map.getZoom()
        const bounds    = map.getBounds()
        const west      = bounds.getWest()
        const south     = bounds.getSouth()
        const east      = bounds.getEast()
        const north     = bounds.getNorth()
        
        var preloadZoom = zoom + 1

        //message
        // console.log('We are displaying zoom ' + zoom + '. We are preloading data from level ' + preloadZoom);

        // Determine which tile we need
        const dataEast  = long2tile(east, preloadZoom)
        const dataWest  = long2tile(west, preloadZoom)
        const dataNorth = lat2tile(north, preloadZoom)
        const dataSouth = lat2tile(south, preloadZoom)
        
        const tilesToPreload = []
        
        for(let y = dataNorth; y < dataSouth + 1; y++) {
            for(let x = dataWest; x < dataEast + 1; x++) {
                tilesToPreload.push(`${BASE_TILE}${TYPE_TILE}/${preloadZoom}/${x}/${y}.png?access_token=${MAP_TOKEN}`)
            }		
        }
        
        preloadImages(tilesToPreload)
            .then(done => {
                // console.log("All images have been loaded");
            })
    })
  }

}

Component.append(MapComponent)