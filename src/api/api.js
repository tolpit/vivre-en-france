import {API_URL} from '../utils/global'

const apiURL = 'https://ressources.data.sncf.com/api/records/1.0/search/?dataset=liste-des-gares&rows=10000&facet=voyageurs&facet=code_ligne&facet=departement&refine.voyageurs=O'

export function getGares() {
    return fetch(`${API_URL}/assets/data/gares.sncf.json`)
        .then(res => res.json())
        .catch(err => console.error(err))
}

export function getCentrales() {
    return fetch(`${API_URL}/assets/data/nucleaire.json`)
        .then(res => res.json())
        .catch(err => console.error(err))
}

export function getCinemas() {
    return fetch(`${API_URL}/assets/data/cinemas.geo.json`)
        .then(res => res.json())
        .catch(err => console.error(err))
}

export function getZonesInondables() {
    return fetch(`${API_URL}/assets/data/zones_inondables/azibretagne.json`)
        .then(res => res.json())
        .catch(err => console.error(err))
}

export function getMonuments() {
    return fetch(`${API_URL}/assets/data/monuments_historiques.json`)
        .then(res => res.json())
        .catch(err => console.error(err))
}


export function getData() {
    let gares = [],
        centrales = [];

    return new Promise(resolve => {
        getGares()
            .then(data => {
                gares = data

                if(!!gares.length && !!centrales.length) resolve({gares,centrales})
            })

        getCentrales()
            .then(data => {
                centrales = data

                if(!!gares.length && !!centrales.length) resolve({gares,centrales})
            })
    })
}