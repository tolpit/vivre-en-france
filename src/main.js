import {observer,mount} from "./core/dom"
import store from "./store"
import localforage from 'localforage'

import MapComponent from './components/map.component.js'
import FiltersComponent from './components/filters.component.js'
import SearchComponent from './components/search.component.js'

import {getData} from './api/api.js'

localforage.config({
    name        : 'autonomie-map',
    version     : 1.0
});

mount()

getData()
    .then(({gares,centrales}) => {
        store.gares = gares
        store.centrales = centrales        
    })

// register service worker
if ('serviceWorker' in navigator) {
    // navigator.serviceWorker.register('/service-worker.js');
}