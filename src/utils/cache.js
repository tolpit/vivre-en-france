// returns approximate size of a single cache (in bytes)
export function cacheSize(c) {
    return c.keys().then(a => {
      return Promise.all(
        a.map(req => c.match(req).then(res => res.clone().blob().then(b => b.size)))
      ).then(a => a.reduce((acc, n) => acc + n, 0));
    });
  }
  
  // returns approximate size of all caches (in bytes)
  export function cachesSize() {
    return caches.keys().then(a => {
      return Promise.all(
        a.map(n => caches.open(n).then(c => cacheSize(c)))
      ).then(a => a.reduce((acc, n) => acc + n, 0));
    });
  }