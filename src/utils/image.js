export function preloadImage(src) {
    return new Promise((resolve, reject) => {
        const image = new Image();

        image.onload  = resolve;
        image.onerror = reject;

        image.src = src;
    });
}

export function preloadImages(srcs) {
    return Promise.all(srcs.map(preloadImage))
        .catch(err => {
            console.error(err);
        });
}