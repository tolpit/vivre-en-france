export const BASE_TILE = "https://api.tiles.mapbox.com/v4/";
export const TYPE_TILE = "mapbox.streets";
export const DEBUG = false
export const API_URL = 'http://localhost:7500'
export const MAP_TOKEN = "pk.eyJ1IjoidGhlby1iYWVzIiwiYSI6ImNqb29tcmt6OTAxM3gzcGxraW5wNGo5eXUifQ.UrYVMaQhX8LDQcCUwjopcQ";
export const log = DEBUG ? console.log.bind(window.console) : () => {};
