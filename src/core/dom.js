let components = [];
let tree       = [];
let DOMIsReady          = false;
let componentsAreReady  = false;

const observer = new MutationObserver(mutations => {
  mutations.forEach(function(mutation) {
    mutation.addedNodes.forEach(node => {
      if(node.nodeName === "#text" || node.nodeName === "#comment") return;

      components.forEach(component => {
        let selectors = component.selector();

        if(!Array.isArray(selectors)) selectors = [selectors];

        try {
          selectors.forEach(selector => {
            if((~selector.indexOf(".") && node.classList.contains(selector.replace('.', ''))) || (~selector.indexOf("#") && node.id === selector.replace('#', ''))) {
              // already mounted
              if(tree.some(component => component.element && component.element.isSameNode(node))) return;

              tree.push(
                new component(node)
              );
            }
          });
        } catch(err) {
          console.error(err);
        }
      });
    });

    mutation.removedNodes.forEach(node => {
      if(node.nodeName === "#text" || node.nodeName === "#comment") return;

      if(tree.some(component => component.element && component.element.isSameNode(node))) {
        const component      = tree.find(component => component.element && component.element.isSameNode(node));
        const componentIndex = tree.findIndex(component => component.element && component.element.isSameNode(node));

        component.beforeDestroy();

        tree.splice(componentIndex, 1);
      }
    })
  });
});

const observerConfig = {
  attributes: false,
  childList: true,
  characterData: false,
  subtree: true
};

observer.observe(document.body, observerConfig);

function bootstrap() {
  if(!DOMIsReady || !componentsAreReady) return;

  components.forEach(component => {
    let selectors = component.selector();

    if(!Array.isArray(selectors)) selectors = [selectors];

    try {
      selectors.forEach(selector => {
        if(document.querySelector(selector)) {
          const elements = document.querySelectorAll(selector);

          elements.forEach(element => {
            // already mounted
            if(tree.some(component => component.element && component.element.isSameNode(element))) return;

            tree.push(
              new component(element)
            );
          });
        }
      });
    } catch(err) {
      console.error(err);
    }

  });
}

export function mount() {
  componentsAreReady = true;

  bootstrap();
}

window.addEventListener('DOMContentLoaded', () => {
  if(DOMIsReady) return;

  DOMIsReady = true;
  
  bootstrap();
});

// TODO: replace with document status = ready
if(document.readyState === "complete" && !DOMIsReady) {
  DOMIsReady = true;
  
  bootstrap();
}

export {components,observer,tree};