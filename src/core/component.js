import {components} from "./dom"
import store        from '../store'

export default class Component {

  static watch() { return []; }

  static hasEvents() { return false; }

  constructor(element) {
    this.element = element;
    this.$store  = store;

    let propsToObserve = this.constructor.watch();

    if(!Array.isArray(propsToObserve) && typeof propsToObserve === "string" && propsToObserve.length > 1) propsToObserve = [propsToObserve];

    if(Array.isArray(propsToObserve) && !!propsToObserve.length) {
      propsToObserve.forEach(key => {
        this.$store.observe(key, this.render.bind(this));
      });
    }

    if(this.constructor.hasEvents()) {
      this.attachEvents();
    }
  }

  attachEvents() {}

  render() {}

  beforeDestroy() {
    delete this.element;
  }

  static append(componentDeclaration) {
    if(components.some(c => c.selector() === componentDeclaration.selector())) return;
    
    components.push(componentDeclaration);
  }

  static register(componentDeclaration) {
    if(components.some(c => c.selector() === componentDeclaration.selector())) return;
    
    components.push(componentDeclaration);
  }

}