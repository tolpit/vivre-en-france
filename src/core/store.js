import {log} from "../utils/global";

export default class Store {

  constructor(state, getters) {
    this.state      = state || {};
    this.getters    = getters;

    this._listeners = {};

    for (let key in this.state) {
      if (this.state.hasOwnProperty(key)) {
        this._makeReactive(this.state, key);
      }
    }
  }

  observe(property, callback) {
    if(!this._listeners[property]) this._listeners[property] = []; // If there is NO signal for the given property, we create it and set it to a new array to store the signalHandlers

    this._listeners[property].push(callback);
  }

  sync() {
    // prevent multiples call to close
    if(this.willSync) {
      clearTimeout(this.willSync);
      this.willSync = null;
    }

    this.willSync = setTimeout(() => {
      log('sync', this.state);
    }, 250);
  }

  restore(state) {
    this.isRestoring = true;

    for(let key in state) {
      if(this.state.hasOwnProperty(key)) this.state[key] = state[key];
    }

    setTimeout(() => this.isRestoring = false, 250);
  }

  _makeReactive(obj, key) {
    const self = this

    Object.defineProperty(self, key, {
      get: (!!self.getters[key] && typeof self.getters[key] === 'function' ? self.getters[key].bind(self) : function() {
        return self.state[key]
      }),
      set: function(value) {
        self.state[key] = value
        self._notify(key);
      }
    });
  }

  _notify(property, newVal) {
    if(!this._listeners[property] || !this._listeners[property].length) return; // Early return if there are no signal handlers

    this._listeners[property].forEach(callback => callback(newVal)); // We call each signalHandler that’s observing the given property
  }

  update(property) {
    this._notify(property, this[property])
  }

}