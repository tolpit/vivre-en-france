import Store from "../core/store.js"

var state = {
    points: [],
    filters: {
        nuclear: 100,
        train: 15,
        flood: null,
        cinema: null,
        bounds: null,
        historique: null
    },
    gares: [],
    garesIds: [],
    centrales: [],
    cinemas: [],
    floods: {},
    monuments: {},
    map: null,
    markers: {},
    infos: {},
    isMobile: window.matchMedia("(max-width: 768px)").matches
}

var getters = {
    
}

export default new Store(state, getters)