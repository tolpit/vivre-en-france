importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js');

workbox.loadModule('workbox-strategies');

workbox.core.setCacheNameDetails({
    prefix: 'osm',
    suffix: 'v1'
});

workbox.precaching.precache([
    { url: 'https://unpkg.com/leaflet@1.6.0/dist/leaflet.css', revision: '1.3.4' },
    { url: 'https://unpkg.com/leaflet@1.3.4/dist/leaflet.js', revision: '1.3.4' },
    // { url: '/src/main.css', revision: '1.0.0' },
    // { url: '/index.html', revision: '1.0.0' },
    // { url: '/dist/bundle.js', revision: '1.0.0' },
]);

workbox.precaching.addRoute();

workbox.routing.registerRoute(
    ({url, event}) => {
        return new RegExp('api\.tiles\.mapbox\.com.*\.png').test(url.href);
    },
    workbox.strategies.staleWhileRevalidate()
);
